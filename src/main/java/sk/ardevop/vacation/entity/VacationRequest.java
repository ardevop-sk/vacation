package sk.ardevop.vacation.entity;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sk.ardevop.vacation.model.VacationRequestRto;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VacationRequest {
  @Id
  @GeneratedValue
  private Long id;
  private String userId;
  private LocalDate date;
  private Integer length;
  private String description;
  private VacationRequestRto.StatusEnum status;
}
