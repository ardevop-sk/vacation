package sk.ardevop.vacation.service;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.ardevop.vacation.entity.VacationRequest;

public interface VacationRequestRepository extends JpaRepository<VacationRequest, Long> {

}
