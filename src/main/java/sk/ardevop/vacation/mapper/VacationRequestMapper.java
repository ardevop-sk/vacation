package sk.ardevop.vacation.mapper;

import sk.ardevop.vacation.entity.VacationRequest;
import sk.ardevop.vacation.model.NewVacationRequestRto;
import sk.ardevop.vacation.model.VacationRequestRto;

public class VacationRequestMapper {

  public static VacationRequestRto toVacationRequestRto(VacationRequest vacationRequest) {
    return new VacationRequestRto().id(vacationRequest.getId())
        .userId(vacationRequest.getUserId())
        .date(vacationRequest.getDate())
        .length(vacationRequest.getLength())
        .description(vacationRequest.getDescription())
        .status(vacationRequest.getStatus());
  }

  public static VacationRequest toVacationRequest(NewVacationRequestRto vacationRequestRto) {
    return VacationRequest.builder()
        .userId(vacationRequestRto.getUserId())
        .date(vacationRequestRto.getDate())
        .length(vacationRequestRto.getLength())
        .description(vacationRequestRto.getDescription())
        .build();
  }
}
