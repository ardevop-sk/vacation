package sk.ardevop.vacation.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

  @ResponseBody
  @GetMapping("/")
  public String index(){
    return "Success";
  }
}
