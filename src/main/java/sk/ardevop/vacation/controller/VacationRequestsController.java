package sk.ardevop.vacation.controller;

import static java.util.stream.Collectors.toList;

import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import sk.ardevop.vacation.api.VacationRequestsApi;
import sk.ardevop.vacation.mapper.VacationRequestMapper;
import sk.ardevop.vacation.model.NewVacationRequestRto;
import sk.ardevop.vacation.model.VacationRequestRto;
import sk.ardevop.vacation.service.VacationRequestRepository;

@RestController
public class VacationRequestsController implements VacationRequestsApi {

  private final VacationRequestRepository vacationRequestRepository;

  public VacationRequestsController(
      VacationRequestRepository vacationRequestRepository) {
    this.vacationRequestRepository = vacationRequestRepository;
  }

  @Override
  public ResponseEntity<VacationRequestRto> addVacationRequest(@Valid NewVacationRequestRto newVacationRequest) {
    return ResponseEntity.ok(
        VacationRequestMapper.toVacationRequestRto(
            vacationRequestRepository.save(
                VacationRequestMapper.toVacationRequest(newVacationRequest))));
  }

  @Override
  public ResponseEntity<VacationRequestRto> findVacationRequestById(Long id) {
    return ResponseEntity.ok(
        VacationRequestMapper.toVacationRequestRto(
            vacationRequestRepository.findById(id).orElseThrow(EntityNotFoundException::new)));
  }

  @Override
  public ResponseEntity<List<VacationRequestRto>> findVacationRequests() {
    return ResponseEntity.ok(
        vacationRequestRepository.findAll().stream()
            .map(VacationRequestMapper::toVacationRequestRto)
            .collect(toList()));
  }


}
